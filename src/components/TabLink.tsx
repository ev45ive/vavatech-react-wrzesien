import * as React from "react";

interface Props {
  selected: boolean;
  onSelect(): void;
}

export const TabLink: React.StatelessComponent<Props> = props => {
  
  return (
    <li className="nav-item">
      <a
        className={`nav-link ${props.selected ? "active" : ""}`}
        href="#"
        onClick={props.onSelect}
      >
        {props.children}
      </a>
    </li>
  );
};
