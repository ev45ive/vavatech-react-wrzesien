import * as React from "react";

export interface Props {
  query: string;
  inputRef?: React.RefObject<HTMLInputElement>;
  onSearch(query: string): void;
  onChange(query: string): void;
}

export class SearchField extends React.Component<Props> {
  change = (event: React.ChangeEvent<HTMLInputElement>) => {
    const query = event.target.value;

    this.props.onChange(query);
    this.search(query);
  };

  debounceTimer: NodeJS.Timer;

  search = (query: string) => {
    clearTimeout(this.debounceTimer);
    this.debounceTimer = setTimeout(() => {
      this.props.onSearch(query);
    }, 400);
  };

  render() {
    return (
      <div>
        <input
          ref={this.props.inputRef}
          type="search"
          className="form-control mb-3"
          value={this.props.query}
          onChange={this.change}
        />
      </div>
    );
  }
}
