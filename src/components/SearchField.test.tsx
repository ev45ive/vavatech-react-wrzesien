import * as React from "react";
import "jest-enzyme";
import { mount, ReactWrapper } from "enzyme";
import { SearchField, Props } from "./SearchField";

describe("Search Field", () => {
  let wrapper: ReactWrapper<Props, {}, SearchField>;
  let query = "batman";
  let searchSpy: jest.Mock;
  let changeSpy: jest.Mock;

  beforeAll(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.clearAllTimers();
  });

  beforeEach(() => {
    searchSpy = jest.fn();
    changeSpy = jest.fn();
    wrapper = mount(
      <SearchField query={query} onChange={changeSpy} onSearch={searchSpy} />
    );
  });

  it("renders", () => {
    expect(wrapper.find(SearchField)).toExist(); // .toHaveLength(1)
  });

  it("should render query value", () => {
    wrapper.setProps({ query: "placki" });
    // expect(wrapper.find("input").prop("value")).toEqual("placki");
    expect(wrapper.find("input")).toHaveValue("placki");
  });

  it("should notify on changes", () => {
    wrapper.find("input").simulate("change", {
      target: { value: "placki" }
    });
    expect(changeSpy).toHaveBeenCalledWith("placki");
  });

  it("should notify on search", () => {
    wrapper.find("input").simulate("change", {
      target: { value: "placki" }
    });
    jest.runAllTimers()
    expect(searchSpy).toHaveBeenCalledWith("placki");
  });

  it("should debounce searches", () => {
    const updateInput = (value: string) =>
      wrapper.find("input").simulate("change", {
        target: { value }
      });

    updateInput("bat");
    // not searching...
    expect(searchSpy).not.toHaveBeenCalled();

    updateInput("batma");
    jest.advanceTimersByTime(399);
    // not searching...
    expect(searchSpy).not.toHaveBeenCalled();

    updateInput("batman");
    jest.advanceTimersByTime(401);
    // searching...
    expect(searchSpy).toHaveBeenCalled();
  });
});
