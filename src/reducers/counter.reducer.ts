import { Reducer, Action, ActionCreator } from "redux";

export type CounterState = number;

export const initialState = 0;

/* Reducer */

export const counter: Reducer<number> = (
  state: CounterState = initialState,
  action: Actions
) => {
  switch (action.type) {
    case "INCREMENT":
      return state + action.payload;
    case "DECREMENT":
      return state - action.payload;
    default:
      return state;
  }
};

/* Action Types */

interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}
interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}
type Actions = INCREMENT | DECREMENT;

/* Action Creators */

export const Increment: ActionCreator<INCREMENT> = (payload = 1) => ({
  type: "INCREMENT",
  payload
});

export const Decrement: ActionCreator<DECREMENT> = (payload = 1) => ({
  type: "DECREMENT",
  payload
});

export const actions = { Increment, Decrement };
