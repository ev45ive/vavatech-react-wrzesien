import { Track } from "../models/Playlist";
import {
  Action,
  ActionCreatorsMapObject,
  Reducer,
  combineReducers
} from "redux";
import { PlaylistsState } from "./playlists.reducer";

type TrackItems = { [id: number]: Track };

export type TracksState = {
  items: TrackItems;
  selected: Track["id"] | null;
};

export const initialState: TracksState = {
  items: {
    123: { id: 123, name: "Track 123" },
    234: { id: 234, name: "Track 234" },
    345: { id: 345, name: "Track 345" }
  },
  selected: null
};

/* Reducer */

const items: Reducer<TrackItems, Actions> = (
  state: TrackItems = initialState.items,
  action
) => {
  switch (action.type) {
    case "TRACK_ADDED": {
      return {
        ...state,
        [action.payload.id]: action.payload
      };
    }
    case "TRACKS_LOADED":
      return action.payload.reduce((items, track) => {
        items[track.id] = track;
        return items;
      }, {});

    default:
      return state;
  }
};

const selected: Reducer<TracksState["selected"], Actions> = (
  state = initialState.selected,
  action
) => {
  switch (action.type) {
    case "TRACK_SELECTED":
      return action.payload.id;
    default:
      return null;
  }
};

export const tracks: Reducer<TracksState, Actions> = combineReducers({
  items,
  selected
});

/* Actions */

interface TRACKS_LOADED extends Action<"TRACKS_LOADED"> {
  payload: Track[];
}

interface TRACK_ADDED extends Action<"TRACK_ADDED"> {
  payload: Track;
}

interface TRACK_SELECTED extends Action<"TRACK_SELECTED"> {
  payload: { id: Track["id"] };
}

interface TRACK_REMOVED extends Action<"TRACK_REMOVED"> {
  payload: { id: Track["id"] };
}

type Actions = TRACK_ADDED | TRACK_REMOVED | TRACKS_LOADED | TRACK_SELECTED;

/* Action Creators */

export const actions: ActionCreatorsMapObject<Actions> = {
  loadTracks(payload: Track[]) {
    return { type: "TRACKS_LOADED", payload };
  },

  selectTrack(id: Track["id"]) {
    return { type: "TRACK_SELECTED", payload: { id } };
  },

  addTrack(payload: Track) {
    return {
      type: "TRACK_ADDED",
      payload
    };
  },
  removeTrack(id: Track["id"]) {
    return {
      type: "TRACK_REMOVED",
      payload: { id }
    };
  }
};

/* Selectors */

export const selectTracks = (
  pstate: PlaylistsState,
  state: TracksState
): Track[] => {
  if (!pstate.selected) {
    return [];
  }
  const ids = pstate.playlistTracks[pstate.selected];
  return ids.map(id => state.items[id]);
};

export const selectSelected = (state: TracksState): Track | undefined =>
  state.selected ? state.items[state.selected] : undefined;
