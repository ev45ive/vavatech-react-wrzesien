import { Action, ActionCreatorsMapObject, Reducer } from "redux";
import { Playlist } from "../models/Playlist";
import { Track } from "src/models/Playlist";

export interface PlaylistsState {
  items: Playlist[];
  selected: Playlist["id"] | null;
  filter: string;
  playlistTracks: {
    [id: number]: Track["id"][];
  };
}

const initialState: PlaylistsState = {
  playlistTracks: {
    123:[123],
    234:[234,345],
    345:[123,234],
  },
  items: [
    {
      id: 123,
      name: "The best of React!",
      favourite: true,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "React Top20",
      favourite: false,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "React Greatest Hits!",
      favourite: true,
      color: "#0000ff"
    }
  ],
  selected: null,
  filter: ""
};

export const playlists: Reducer<PlaylistsState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_LOADED": {
      return { ...state, items: action.payload };
    }
    case "PLAYLISTS_SELECT": {
      return { ...state, selected: action.payload };
    }
    case "PLAYLISTS_SEARCH_START": {
      return { ...state, filter: action.payload.query };
    }
    case "PLAYLISTS_UPDATE": {
      return {
        ...state,
        items: state.items.map(p => {
          return p.id == action.payload.id ? action.payload : p;
        })
      };
    }
    default:
      return state;
  }
};

/* Actions */

interface LOADED extends Action<"PLAYLISTS_LOADED"> {
  payload: Playlist[];
}

interface SELECT extends Action<"PLAYLISTS_SELECT"> {
  payload: Playlist["id"] | null;
}

interface SEARCH_START extends Action<"PLAYLISTS_SEARCH_START"> {
  payload: { query: string };
}

interface SEARCH_SUCCESS extends Action<"PLAYLISTS_SEARCH_SUCCESS"> {
  payload: { results: Playlist[] };
}

interface UPDATE extends Action<"PLAYLISTS_UPDATE"> {
  payload: Playlist;
}

type Actions = LOADED | SELECT | SEARCH_START | SEARCH_SUCCESS | UPDATE;

/* Action Creators */

export const actions: ActionCreatorsMapObject<Actions> = {
  loaded(payload: Playlist[]) {
    return { type: "PLAYLISTS_LOADED", payload };
  },

  select(id?: Playlist["id"]) {
    return { type: "PLAYLISTS_SELECT", payload: id || null };
  },

  search(query: string) {
    return {
      type: "PLAYLISTS_SEARCH_START",
      payload: {
        query
      }
    };
  },

  update(payload: Playlist) {
    return { type: "PLAYLISTS_UPDATE", payload };
  }
};

/* Selectors */

export const selectPlaylists = (state: PlaylistsState) =>
  state.items.filter(p =>
    p.name.toLowerCase().includes(state.filter.toLocaleLowerCase())
  );

export const selectSelectedPlaylist = (state: PlaylistsState) =>
  state.items.find(p => p.id == state.selected);

export const selectFilter = (state: PlaylistsState) => state.filter;
