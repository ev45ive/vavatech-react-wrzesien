import { Album } from "src/models/Album";
import { Reducer, Action, Dispatch } from "redux";
import { musicService } from "src/services";
import { AppState } from "../store";

export interface AlbumsState {
  query: string;
  items: Album[];
  loading: boolean;
}

const initialState: AlbumsState = {
  query: "",
  loading: false,
  items: []
};

export const albums: Reducer<AlbumsState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "ALBUMS_SET_QUERY": {
      let query = action.payload.query;
      return {
        ...state,
        query
      };
    }
    case "ALBUMS_SEARCH_START": {
      let query = action.payload.query;
      return {
        ...state,
        query,
        loading: true
      };
    }
    case "ALBUMS_SEARCH_SUCCESS": {
      return {
        ...state,
        items: action.payload.results,
        loading: false
      };
    }
    default:
      return state;
  }
};

type Actions =
  | ALBUMS_SET_QUERY
  | ALBUMS_SEARCH_START
  | ALBUMS_SEARCH_SUCCESS
  | ALBUMS_SEARCH_FAIL;

interface ALBUMS_SET_QUERY extends Action<"ALBUMS_SET_QUERY"> {
  payload: {
    query: string;
  };
}

interface ALBUMS_SEARCH_START extends Action<"ALBUMS_SEARCH_START"> {
  payload: {
    query: string;
  };
}

interface ALBUMS_SEARCH_SUCCESS extends Action<"ALBUMS_SEARCH_SUCCESS"> {
  payload: {
    results: Album[];
  };
}

interface ALBUMS_SEARCH_FAIL extends Action<"ALBUMS_SEARCH_FAIL"> {
  payload: {
    error: string;
  };
}

/* Action Creators */

export const actions = {
  updateQuery(query: string) {
    return {
      type: "ALBUMS_SET_QUERY",
      payload: { query }
    };
  },

  searchStart(query: string) {
    return {
      type: "ALBUMS_SEARCH_START",
      payload: {
        query
      }
    };
  },

  searchSuccess(albums: Album[]) {
    return {
      type: "ALBUMS_SEARCH_SUCCESS",
      payload: {
        results: albums
      }
    };
  },

  onSearch: (query: string) => (
    dispatch: Dispatch,
    getState: () => AppState
  ) => {
    dispatch(actions.searchStart(query));

    musicService.fetchAlbums(query).then(albums => {
      dispatch(actions.searchSuccess(albums));
    });
  }
};

/* Selectors */

export const selectors = {
  albums(state: AlbumsState) {
    return state.items;
  },
  query(state: AlbumsState) {
    return state.query;
  },
  loading(state: AlbumsState) {
    return state.loading;
  }
};
