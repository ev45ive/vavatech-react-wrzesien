import {
  createStore,
  combineReducers,
  applyMiddleware,
} from "redux";
import { TracksState, tracks } from "./reducers/tracks.reducer";
import { CounterState, counter } from "src/reducers/counter.reducer";
import { playlists, PlaylistsState } from "./reducers/playlists.reducer";
import { AlbumsState, albums } from "./reducers/albums.reducer";

export interface AppState {
  counter: CounterState;
  nested: {
    counter: CounterState;
  };
  tracks: TracksState;
  playlists: PlaylistsState;
  albums: AlbumsState;
}

const reducer = combineReducers<AppState>({
  counter,
  nested: combineReducers({ counter }),
  tracks,
  playlists,
  albums
});



import thunk from "redux-thunk";

export const store = createStore(reducer, applyMiddleware(thunk));

window["store"] = store;
