import * as React from "react";
import { ReactWrapper, mount } from "enzyme";
import {
  PlaylistDetails,
  PlaylistDetailsProps,
  PlaylistDetailsState
} from "./PlaylistDetails";
import { Playlist } from "src/models/Playlist";

describe("Playlist Details Component", () => {
  let wrapper: ReactWrapper<
    PlaylistDetailsProps,
    PlaylistDetailsState,
    PlaylistDetails
  >;
  let playlist: Playlist;
  let saveSpy: jest.Mock;

  beforeEach(() => {
    saveSpy = jest.fn();
    playlist = {
      id: 123,
      name: "Test 123",
      favourite: false,
      color: "#ff0000"
    };
    wrapper = mount<PlaylistDetails, PlaylistDetailsProps>(
      <PlaylistDetails selected={playlist} onSave={saveSpy} />
    );
  });

  it("render", () => {
    expect(wrapper).toHaveLength(1);
  });

  it("should render playlist details", () => {
    const name = wrapper.find(".playlist-name");
    const favourite = wrapper.find(".playlist-favourite");
    const color = wrapper.find(".playlist-color");

    expect(name.text()).toContain(playlist.name);
    expect(favourite.text()).toContain("No");
    expect(color.text()).toContain(playlist.color);
  });

  it("should switch mode when edit is clicked", () => {
    expect(wrapper.state("editing")).toBeFalsy();
    const editBtn = wrapper.find('input[value="Edit"]');

    editBtn.simulate("click");
    expect(wrapper.state("editing")).toBeTruthy();
  });

  it("should enable playlist editing", () => {
    wrapper.setState({ editing: true });

    const saveBtn = wrapper.find('input[value="Save"]');
    expect(saveBtn).toHaveLength(1);
  });

  it('should edit "name"', () => {
    wrapper.setState({ editing: true });

    const nameInput = wrapper.find('input[name="name"]');
    expect(nameInput.prop("value")).toEqual(playlist.name);

    nameInput.simulate("change", {
      target: {
        name: "name",
        value: "Placki"
      }
    });
    expect(wrapper.state("playlist")).toHaveProperty("name", "Placki");
  });

  it("should save playlist", () => {
    const updated = { ...playlist, name: "Zmienione" };

    wrapper.setState({ editing: true, playlist: updated });

    // wrapper.instance().save()
    const saveBtn = wrapper.find('input[value="Save"]');
    saveBtn.simulate("click");

    expect(saveSpy).toHaveBeenCalledWith(updated);
  });
});
