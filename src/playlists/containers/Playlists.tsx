import { connect } from "react-redux";
import PlaylistsView, { PlaylistsProps, DispatchProps } from "../PlaylistsView";
import { AppState } from "../../store";
import {
  selectFilter,
  selectPlaylists,
  selectSelectedPlaylist,
  actions
} from "../../reducers/playlists.reducer";
import { bindActionCreators } from "redux";

export const Playlists = connect<PlaylistsProps, DispatchProps, {}, AppState>(
  state => ({
    filter: selectFilter(state.playlists),
    playlists: selectPlaylists(state.playlists),
    selected: selectSelectedPlaylist(state.playlists)
  }),
  dispatch =>
    bindActionCreators(
      {
        select: p => actions.select(p.id),
        save: actions.update,
        search: actions.search,
        updateFilter: actions.search
      },
      dispatch
    )
)(PlaylistsView);
