import * as React from "react";
import { Playlist } from "../models/Playlist";
import { Fragment } from "react";

export interface PlaylistDetailsState {
  playlist: Playlist;
  editing: boolean;
}
export interface PlaylistDetailsProps {
  selected: Playlist;
  onSave(playlist: Playlist): void;
}

export class PlaylistDetails extends React.Component<
  PlaylistDetailsProps,
  PlaylistDetailsState
> {
  state: PlaylistDetailsState = {
    playlist: this.props.selected,
    editing: false
  };

  save = () => {
    this.props.onSave(this.state.playlist);
    this.setState({
      editing: false
    });
  };

  edit = () => {
    this.setState({
      editing: true
      // playlist: this.props.selected
    });
  };

  componentDidMount() {
    //console.log("componentDidMount");
  }

  // componentWillReceiveProps
  static getDerivedStateFromProps(
    props: PlaylistDetailsProps,
    state: PlaylistDetailsState
  ) {
    //console.log("getDerivedStateFromProps", props, state);

    return {
      playlist:
        props.selected.id !== state.playlist.id
          ? props.selected
          : state.playlist
    };
  }

  shouldComponentUpdate(
    nextProps: PlaylistDetailsProps,
    nextState: PlaylistDetailsState
  ) {
    //console.log("shouldComponentUpdate");

    return (
      this.props.selected.id !== nextProps.selected.id ||
      nextState != this.state
    );
  }

  getSnapshotBeforeUpdate() {
    //console.log("getSnapshotBeforeUpdate");
    return { placki: 123 };
  }

  componentDidUpdate(props: any, state: any, snapshot: any) {
    //console.log("componentDidUpdate", snapshot);
  }

  componentWillUnmount() {
    //console.log("componentWillUnmount");
  }

  handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    const fieldName = target.name;
    const value = target.type == "checkbox" ? target.checked : target.value;

    // this.state.playlist[fieldName] = value
    // this.setState({})

    this.setState({
      playlist: {
        ...this.state.playlist,
        [fieldName]: value
      }
    });
  };
  render() {
    //console.log("details render");
    const playlist = this.state.playlist;
    const selected = this.props.selected;
    return (
      <Fragment>
        {!this.state.editing && (
          <div>
            <dl>
              <dt>Name:</dt>
              <dd className="playlist-name">{selected.name} </dd>
              <dt>Favourite:</dt>
              <dd className="playlist-favourite">{selected.favourite ? "Yes" : "No"} </dd>
              <dt>Color:</dt>
              <dd  className="playlist-color" style={{ color: selected.color }}>{selected.color}</dd>
            </dl>
            <input
              type="button"
              value="Edit"
              onClick={this.edit}
              className="btn btn-info"
            />
          </div>
        )}

        {this.state.editing && (
          <div>
            {/* .form-group*3>label+input.form-control */}

            <div className="form-group">
              <label>Name:</label>
              <input
                type="text"
                className="form-control"
                onChange={this.handleFieldChange}
                value={playlist.name}
                name="name"
              />
            </div>

            <div className="form-group">
              <label>Favourite</label>
              <input
                type="checkbox"
                onChange={this.handleFieldChange}
                checked={playlist.favourite}
                name="favourite"
              />
            </div>

            <div className="form-group">
              <label>Color</label>
              <input
                type="color"
                value={playlist.color}
                onChange={this.handleFieldChange}
                name="color"
              />
            </div>

            <input
              type="button"
              value="Save"
              onClick={this.save}
              className="btn btn-success"
            />
          </div>
        )}
      </Fragment>
    );
  }
}
