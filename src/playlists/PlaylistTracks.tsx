import * as React from "react";
import { TracksList } from "./TracksList";
import { Track } from "../models/Playlist";
import { Playlist } from "src/models/Playlist";

export interface Props {
  tracks: Track[];
  playlist?: Playlist;
  selectedTrack?: Track;
}

export interface DispatchProps {
  remove(id: Track["id"]): void;
  select(track: Track["id"]): void;
  save(playlist: Playlist): void;
}

export class PlaylistTracksView extends React.Component<Props & DispatchProps> {
  select = (track: Track) => {
    this.props.select(track.id);
  };

  save = (playlist: Playlist) => {
    this.props.save(playlist);
  };

  remove = (id: Track["id"]) => {
    this.props.remove(id);
  };

  render() {
    return (
      <div className="row">
        {this.props.playlist && (
          <div className="col">
            <PlaylistDetails
              onSave={this.save}
              selected={this.props.playlist}
            />
          </div>
        )}
        <div className="col">
          <TracksList
            onRemove={this.remove}
            onSelect={this.select}
            selected={this.props.selectedTrack}
            tracks={this.props.tracks}
          />
        </div>
      </div>
    );
  }
}

/* === */
import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { AppState } from "../store";
import { Dispatch, bindActionCreators } from "redux";
import {
  selectSelectedPlaylist,
  actions as playlistActions
} from "../reducers/playlists.reducer";
import {
  actions,
  selectTracks,
  selectSelected
} from "../reducers/tracks.reducer";
import { PlaylistDetails } from "./PlaylistDetails";

const mapStateToProps: MapStateToProps<Props, {}, AppState> = state => ({
  playlist: selectSelectedPlaylist(state.playlists),
  tracks: selectTracks(state.playlists,state.tracks),
  selectedTrack: selectSelected(state.tracks)
});

// const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
//   dispatch: Dispatch
// ) => ({
//   remove: id => dispatch(actions.removeTrack(id)),
// });

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
  dispatch: Dispatch
) =>
  bindActionCreators(
    {
      remove: actions.removeTrack,
      select: actions.selectTrack,
      save: playlistActions.update
    },
    dispatch
  );

export const PlaylistTracks = connect(
  mapStateToProps,
  mapDispatchToProps
  /* mergeProps */
)(PlaylistTracksView);
