import * as React from "react";
import { Track } from "../models/Playlist";

interface Props {
  tracks: Track[];
  selected?: Track;
  onSelect(track: Track): void;
  onRemove(id: Track["id"]): void;
}

export class TracksList extends React.Component<Props> {
  render() {
    return (
      <div className="list-group">
        {this.props.tracks.map(track => (
          <div
            className={`list-group-item ${
              this.props.selected == track ? "active" : ""
            }`}
            onClick={() => this.props.onSelect(track)}
            key={track.id}
          >
            {track.name}

            <span
              className="close remove"
              onClick={() => this.props.onRemove(track.id)}
            >
              &times;
            </span>
          </div>
        ))}
      </div>
    );
  }
}
