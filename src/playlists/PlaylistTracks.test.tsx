/* import { ReactWrapper, mount } from "enzyme";
import { State, PlaylistTracks } from "./PlaylistTracks";
import * as React from "react";
import { TracksList } from "./TracksList";
import { PlaylistDetails } from "./PlaylistDetails";
import { Playlist, Track } from "src/models/Playlist";

describe("Playlist Tracks Component", () => {
  // let wrapper: ShallowWrapper<{}, State, PlaylistTracks>;
  let wrapper: ReactWrapper<{}, State, PlaylistTracks>;
  let playlist: Playlist, tracks: Track[];

  beforeEach(() => {
    // wrapper = shallow(<PlaylistTracks />);
    wrapper = mount(<PlaylistTracks />);
    playlist = {
      id: 123,
      name: "test",
      favourite: false,
      color: "#ff0000"
    };
    playlist;
    tracks = [
      { id: 123, name: "Track 123" },
      { id: 234, name: "Track 234" },
      { id: 345, name: "Track 345" }
    ];
  });

  it("should render", () => {
    expect(wrapper.find(PlaylistTracks)).toHaveLength(1);
  });

  it("should render track list", () => {
    expect(wrapper.find(TracksList)).toHaveLength(1);
  });

  it("should render playlist details", () => {
    expect(wrapper.find(PlaylistDetails)).toHaveLength(1);
  });

  it("should select tracks", () => {
    const instance = wrapper.instance();
    const track = tracks[0];
    instance.select(track);
    wrapper.update();

    expect(wrapper.state("selectedTrack")).toEqual(track);
    expect(wrapper.find(TracksList).prop("selected")).toEqual(track);
  });

  it("should remove tracks", () => {
    const instance = wrapper.instance();
    const tracks = wrapper.state("tracks");
    const track = tracks[0];
    expect(wrapper.state("tracks")).toContain(track);

    instance.remove(track.id);
    // wrapper.update();

    expect(wrapper.state("tracks")).not.toContain(track);
  });
});
 */