import { mount, ReactWrapper } from "enzyme";
import { TracksList } from "./TracksList";
// import { render, shallow, mount} from "enzyme";
import * as React from "react";

describe("TracksList Component", () => {
  let tracks = [
    { id: 1, name: "Track 1" },
    { id: 2, name: "Track 2" },
    { id: 3, name: "Track 3" }
  ];
  let wrapper: ReactWrapper;
  let selectSpy: jest.Mock;
  let removeSpy: jest.Mock;

  beforeEach(() => {
    selectSpy = jest.fn();
    removeSpy = jest.fn();

    wrapper = mount(
      <TracksList onRemove={removeSpy} onSelect={selectSpy} tracks={tracks} />
    );
  });

  it("renders", () => {
    expect(wrapper.find(TracksList)).toHaveLength(1);
  });

  it("renders a list", () => {
    const items = wrapper.find(".list-group-item");
    expect(items).toHaveLength(tracks.length);
  });

  it("should highlight only the selected item", () => {
    const index = 0;
    wrapper.setProps({ selected: tracks[index] });

    const items = wrapper.find(".list-group-item");

    const activeItems = items.filterWhere(item => item.hasClass("active"));

    // console.log(wrapper.debug());

    expect(activeItems).toHaveLength(1);
    expect(items.at(index)).toEqual(activeItems.first());
  });

  it("should select track on click", () => {
    wrapper.setProps({
      onSelect: selectSpy
    });

    const index = 0;

    wrapper
      .find(".list-group-item")
      .at(index)
      .simulate("click");

    expect(selectSpy).toHaveBeenCalledWith(tracks[index]);
  });

  it("should remove items", () => {
    const index = 0;
    const item = wrapper.find(".list-group-item").at(index);
    const btn = item.find(".remove");
    btn.simulate("click");

    expect(removeSpy).toHaveBeenCalledWith(tracks[index].id);
  });
});
