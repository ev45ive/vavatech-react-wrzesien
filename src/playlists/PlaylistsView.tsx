import * as React from "react";
import { PlaylistDetails } from "./PlaylistDetails";
import { Playlist } from "../models/Playlist";
import { PlaylistsList } from "../playlists/PlaylistsList";
import { SearchField } from "../components/SearchField";

export interface PlaylistsProps {
  playlists: Playlist[];
  selected?: Playlist;
  filter: string;
}

export interface DispatchProps {
  select(selected: Playlist): void;
  save(playlist: Playlist): void;
  search(query: string): void;
  updateFilter(filter: string): void;
}

class PlaylistsView extends React.Component<PlaylistsProps & DispatchProps> {
  public render() {
    return (
      <div className="row">
        <div className="col">
          <SearchField
            query={this.props.filter}
            onChange={this.props.updateFilter}
            onSearch={this.props.search}
          />

          <PlaylistsList
            onSelect={this.props.select}
            selected={this.props.selected}
            playlists={this.props.playlists}
          />
        </div>
        <div className="col">
          {this.props.selected ? (
            <PlaylistDetails
              onSave={this.props.save}
              selected={this.props.selected}
            />
          ) : (
            <p>Please select playlist</p>
          )}
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
