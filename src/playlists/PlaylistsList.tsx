import * as React from "react";
import { Playlist } from "../models/Playlist";

export interface PlaylistListProps {
  playlists: Playlist[];
  selected?: Playlist;
  onSelect(playlist: Playlist): void;
}

export class PlaylistsList extends React.PureComponent<PlaylistListProps> {
  select = (playlist: Playlist) => {
    this.props.onSelect(playlist);
  };

  render() {
    return (
      <div className="list-group">
        {this.props.playlists.map(playlist => (
          <div
            className={`list-group-item ${
              this.props.selected == playlist ? "active" : ""
            }`}
            key={playlist.id}
            onClick={() => this.select(playlist)}
          >
            {playlist.name}
          </div>
        ))}
      </div>
    );
  }
}
