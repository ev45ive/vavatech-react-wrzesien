export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * Color in rbg hex format
   */
  color: string;
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}
