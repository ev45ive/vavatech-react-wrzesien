interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
  height: number;
  width: number;
}

export interface PagingObject<T> {
  items: T[];
}

export type AlbumsResponse = {
  albums: PagingObject<Album>;
};
