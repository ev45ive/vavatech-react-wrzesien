import { connect } from "react-redux";
import { MusicSearchView } from "../MusicSearchView";
import { AppState } from "../../store";
import { selectors, actions } from "../../reducers/albums.reducer";
import { bindActionCreators } from "redux";
import { RouteComponentProps } from "react-router";
// import { bindActionCreators } from "redux";

export const MusicSearch = connect(
  (state: AppState, ownProps: RouteComponentProps<{}>) => {
    return {
      query: selectors.query(state.albums),
      items: selectors.albums(state.albums),
      loading: selectors.loading(state.albums),
      ...ownProps
    };
  },
  dispatch =>
    bindActionCreators(
      {
        onSearch: actions.onSearch,
        updateQuery: actions.updateQuery
      },
      dispatch
    )
  /* mergeProps */
  // https://www.npmjs.com/package/react-router-redux
  /* (stateProps, dispatchProps, ownProps: RouteComponentProps<{}>) => {
    const params = new URLSearchParams(ownProps.location.search);
    const query = params.get("query");

    if (query && query !== ownProps.location.search) {
      stateProps.query = query;
    }
    return {
      ...stateProps,
      ...dispatchProps
    };
  } */
)(MusicSearchView);
