import * as React from "react";
import { Album } from "../models/Album";

export interface CardsGridProps {
  items: Album[];
}

export default class CardsGrid extends React.Component<CardsGridProps, any> {
  public render() {
    return (
      <div className="card-group">
        {this.props.items.map(item => (
          <GridCard item={item} key={item.id} />
        ))}
      </div>
    );
  }
}

interface GridCardProps {
  item: Album;
}

export const GridCard: React.StatelessComponent<GridCardProps> = props => (
  <div className="card" style={{ flex: "0 0 25%" }}>
    <img className="card-img-top" src={props.item.images[0].url} />
    <div className="card-body">
      <h5 className="card-title">{props.item.name}</h5>
    </div>
  </div>
);
