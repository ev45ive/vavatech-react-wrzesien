import * as React from "react";
import { SearchField } from "../components/SearchField";
import CardsGrid from "./CardsGrid";
import { Album } from "src/models/Album";
import { RouteComponentProps } from "react-router-dom";

export interface Props extends RouteComponentProps<{}> {
  query: string;
  items: Album[];
  loading: boolean;
}

export interface DispatchProps {
  onSearch(query: string): void;
  updateQuery(query: string): void;
}

export class MusicSearchView extends React.Component<Props & DispatchProps> {

  // https://www.npmjs.com/package/react-router-redux
  componentDidUpdate(prevProps: RouteComponentProps<{}>) {
    if (this.props.location.search !== prevProps.location.search) {
      const params = new URLSearchParams(this.props.location.search);
      const query = params.get("query");
      if (query) {
        this.props.updateQuery(query);
      }
    }
  }

  componentDidMount(){
    const params = new URLSearchParams(this.props.location.search);
      const query = params.get("query");
      if (query) {
        this.props.onSearch(query);
      }
  }

  updateQuery = (query: string) => {
    this.props.history.push({
      search: "?query=" + query
    });
  };

  render() {
    return (
      <>
        <div className="row">
          <div className="col">
            <SearchField
              query={this.props.query}
              onChange={this.updateQuery}
              onSearch={this.props.onSearch}
            />
          </div>
        </div>

        <div className="row">
          <div className="col">
            {this.props.loading ? (
              <div style={{ textAlign: "center" }}>
                <p>Please wait</p>
                <div className="lds-circle" />
              </div>
            ) : (
              <CardsGrid items={this.props.items} />
            )}
          </div>
        </div>
      </>
    );
  }
}
