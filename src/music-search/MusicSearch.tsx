import * as React from "react";
import { MusicApiContext } from "../music-search/MusicApi";
import { MusicApi } from "./MusicApi";

// export class MusicSearch extends React.Component {
//   render() {
//     return (
//       <MusicApiContext.Consumer>
//         {music => music && <MusicSearchView onSearch={music.fetchAlbums} />}
//       </MusicApiContext.Consumer>
//     );
//   }
// }


type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
type Subtract<T, K> = Omit<T, keyof K>;

export const withMusicApi = <P extends {}, ApiProps>(
  mapApiToProps: (api: MusicApi) => ApiProps,
  Component: React.ComponentType<P & ApiProps>
) =>
  class WithMusic extends React.Component<Subtract<P, ApiProps>> {
    render() {
      return (
        <MusicApiContext.Consumer>
          {music =>
            music && <Component {...this.props} {...mapApiToProps(music)} />
          }
        </MusicApiContext.Consumer>
      );
    }
  };

// https://medium.com/@jrwebdev/react-higher-order-component-patterns-in-typescript-42278f7590fb

/* export const MusicSearch = withMusicApi(
  api => ({
    onSearch: api.fetchAlbums
  }),
  MusicSearchView
);

 */