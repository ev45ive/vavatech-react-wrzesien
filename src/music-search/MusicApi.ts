import { AlbumsResponse } from "src/models/Album";
import { Security } from "src/security/Security";
import * as React from "react";

export class MusicApi {
  constructor(private security: Security) {}

  fetchAlbums = (query: string = "batman") => {
    return fetch(
      `https://api.spotify.com/v1/search?type=album&query=${query}`,
      {
        headers: {
          Authorization: `Bearer ${this.security.getToken()}`
        }
      }
    )
      .then<AlbumsResponse>(response => response.json())
      .then(data => {
        return data.albums.items;
      });
  }
}

export const MusicApiContext = React.createContext<MusicApi | null>(null);
