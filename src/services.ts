import { Security } from "./security/Security";
import { MusicApi } from "./music-search/MusicApi";

export const securityService = new Security(
  "https://accounts.spotify.com/authorize",
  "fa2709b7c8d1414885c0280d16f408f7",
  "http://localhost:3000/"
);
securityService.getToken();

export const musicService = new MusicApi(securityService);
