import * as React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { Layout } from "./components/Layout";
import { Route, Redirect, Switch } from "react-router-dom";
import { Playlists } from "./playlists/containers/Playlists";
import { MusicSearch } from "./music-search/containers/MusicSearch";

export class App extends React.Component {
  public render() {
    return (
      <Layout>
        <Switch>
          <Route path="/playlists" component={Playlists} />
          <Route path="/music" component={MusicSearch} />
          <Redirect from="/" to="playlists" exact={true} />
          <Route
            path="*"
            render={() => <h1 className="text-center">404 - Nothing here ...</h1>}
          />
        </Switch>
      </Layout>
    );
  }
}

export default App;
