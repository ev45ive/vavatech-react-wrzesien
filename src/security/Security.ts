import * as React from "react";
export class Security {
  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type: string = "token"
  ) {}

  authorize() {
    const url = `${this.auth_url}?client_id=${this.client_id}&redirect_uri=${
      this.redirect_uri
    }&response_type=${this.response_type}`;

    sessionStorage.removeItem("token");

    // Redirect to auth_url:
    location.replace(url);
  }

  private token = "";

  extractToken() {
    const match = location.hash.match(/access_token=([^&]*)/);
    location.hash = "";
    return match && match[1];
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token") || "null");

    if (!this.token) {
      this.token = this.extractToken() || "";

      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}

export const SecurityContext = React.createContext<Security | null>(null);
